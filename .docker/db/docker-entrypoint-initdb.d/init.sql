drop table if exists urls;
create table urls
(
    id bigint unsigned auto_increment primary key,
    slug varchar(6) not null,
    url varchar(500) not null,
    count integer,
    constraint urls_slug_unique unique (slug)
);
