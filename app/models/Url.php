<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @var integer $id
 * @var string $slug
 * @var string $url
 * @var integer $count
 */
class Url extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%urls}}';
    }

    public function rules()
    {
        return [
            ['slug', 'string'],
            ['url', 'string'],
            ['count', 'integer'],
        ];
    }
}