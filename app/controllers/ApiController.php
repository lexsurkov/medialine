<?php

namespace app\controllers;

use app\models\Url;
use Yii;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class ApiController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats'] = [
            'application/json' => Response::FORMAT_JSON,
        ];
        return $behaviors;
    }

    public function actionIndex($slug)
    {
        return Url::find()
            ->select(['url', 'count'])
            ->where(['slug' => $slug])
            ->one();
    }

    public function actionCreate()
    {
        $url = Yii::$app->request->getBodyParam('url');
        $slug = substr(md5(uniqid($url)), 0, 6);

        $model = new Url();
        $model->slug = $slug;
        $model->url = $url;
        if (!$model->save()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        $response = Yii::$app->getResponse();
        $response->setStatusCode(201);
        return Yii::$app->urlManager->createAbsoluteUrl($slug);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'create' => ['POST'],
        ];
    }
}