<?php

namespace app\controllers;

use app\models\Url;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays homepage.
     *
     * @return \yii\web\Response
     */
    public function actionSlug($slug)
    {
        $url = Url::find()
            ->where(['slug' => $slug])
            ->one();

        if (!$url) {
            return $this->goHome();
        }

        Url::updateAll(['count' => $url->count + 1], ['slug' => $slug]);

        return $this->redirect($url->url);
    }

}
